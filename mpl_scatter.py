import matplotlib.pyplot as plt

x = list(range(0, 1000+1))
y = [i*i for i in x]
plt.style.use("ggplot")
fig, ax = plt.subplots()
ax.scatter(x, y, c = y, cmap = plt.cm.Blues, s = 1)

#set chart title and label axes
ax.set_title("Square numbers", fontsize = 24)
ax.set_xlabel("Value", fontsize = 14)
ax.set_ylabel("Square of value", fontsize = 24)

#set size of tick labels
ax.tick_params(axis = "both", which = "major", labelsize = 14)

#set the range for each axis
ax.axis([min(x), max(x), min(y), max(y)])

plt.show()
