from die import Die
from plotly.graph_objs import Bar, Layout
from plotly import offline

# Create a 6 sided die
die_1 = Die()
die_2 = Die(10)

# Make some rolls, store their results in a list
results = []

for num in range(0, 9999+1):
    result = die_1.roll() + die_2.roll()
    results.append(result)

# See how many times each number came

frequencies = []
max_result = die_1.num_sides + die_2.num_sides
for num in range(2, max_result + 1):
    frequencies.append(results.count(num))

# Visualize the results
x_values = list(range(2, max_result + 1))
data = [Bar(x = x_values, y = frequencies)]

x_axis_config = {"title" : "Result", "dtick" : 1}
y_axis_config = {"title" : "Frequency of result"}

my_layout = Layout(title = "Results of rolling one fair 6 sided die and oen fair 10 sided die  10000 times",
                   xaxis = x_axis_config, yaxis = y_axis_config)

offline.plot({"data" : data, "layout" : my_layout}, filename = "D6_D10.html")

