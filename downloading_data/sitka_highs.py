import csv

import matplotlib.pyplot as plt
from datetime import datetime


filename = "./data/sitka_weather_2018_simple.csv"

with open(filename) as csv_file:
    reader = csv.reader(csv_file)
    header_row = next(reader)

    # Get maximum temperature, minimum temperature and dates of each row
    max_temp, min_temp, dates = [], [], []
    for row in reader:
        dates.append(datetime.strptime(row[2], "%Y-%m-%d"))
        max_temp.append(int(row[5]))        #because the 5th index contains TMAX
        min_temp.append(int(row[6]))

# Plot the high temperatures

plt.style.use("ggplot")

fig, axes = plt.subplots(figsize = (19, 10))
axes.plot(dates, max_temp, c = "red", alpha = 0.5)
axes.plot(dates, min_temp, c = "blue", alpha = 0.5)
plt.fill_between(dates, max_temp, min_temp, facecolor = "blue", alpha = 0.1)

# Format plot
plt.xlabel("Date", fontsize = 14)
fig.autofmt_xdate()
plt.ylabel("Temperature", fontsize = 14)
plt.title("Daily maximum and minimum temperatures - Sitka, 2018", fontsize = 24)
plt.tick_params(axis = "both", which = "major", labelsize = 14)

plt.show()
