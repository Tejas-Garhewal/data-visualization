import csv
import matplotlib.pyplot as plt
from datetime import datetime

filename = "./data/death_valley_2018_simple.csv"

with open(filename) as csvFile:
    reader = csv.reader(csvFile)
    min_temp, max_temp, dates = [], [], []
    row_num = 0
    for row in reader:
        row_num += 1
        try:
            current_date = datetime.strptime(row[2], "%Y-%m-%d")
            high = int(row[4])
            low = int(row[5])
        except ValueError:
            print(f"Issue converting current_date in row {row_num}, ignoring row.")
        else:
            dates.append(current_date)
            max_temp.append(high)
            min_temp.append(low)

fig, axes = plt.subplots(figsize = (19, 10))
axes.plot(dates, max_temp, alpha = 0.5, c = "red")
axes.plot(dates, min_temp, alpha = 0.5, c = "blue")
plt.fill_between(dates, max_temp, min_temp, facecolor = "blue", alpha = 0.1)

plt.xlabel("Date", fontsize = 14)
fig.autofmt_xdate()
plt.ylabel("temperature", fontsize = 14)
plt.title("Temperatures in Death Valley, 2018", fontsize = 24)
plt.tick_params(which = "major", axis = "both", labelsize = 14)

plt.show()

