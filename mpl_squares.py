import matplotlib.pyplot as plt

input_values = [i for i in range(1, 10 + 1)]
squares = [i*i  for i in range(1, 10 + 1)]
#print(squares)
plt.style.use("ggplot")
fig, ax = plt.subplots()
ax.plot(input_values, squares, linewidth = 3)

#set chart title and label axes
ax.set_title("Square numbers", fontsize = 24)
ax.set_xlabel("Value", fontsize = 14)
ax.set_ylabel("Square of value", fontsize = 24)

#set size of tick labels
ax.tick_params(axis = "both", labelsize = 14)

plt.show()
