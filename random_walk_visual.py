import matplotlib.pyplot as plt


from random_walk import RandomWalk

#Keep making the walks until user says no
while True:
# Make a randomwalk

    rw = RandomWalk(500_000)
    rw.fill_walk()

    num_points = range(rw.num_points)

    # Plot the points in the walk
    plt.style.use("ggplot")
    fig, axes = plt.subplots(figsize = (19, 10))
    # Plot the values as a scatterplot
    axes.scatter(rw.x_values, 
                 rw.y_values, 
                 c = num_points, 
                 cmap = plt.cm.Blues,
                 edgecolors = "none",
                 s = 1)
    # Emphasize the first pair of points
    axes.scatter(rw.x_values[0],
                 rw.y_values[0],
                 c = "green",
                 edgecolors = "none",
                 s = 100)
    # Emphasize the last pair of points
    axes.scatter(rw.x_values[-1],
                 rw.y_values[-1],
                 c = "red",
                 edgecolors = "none",
                 s = 100)

    # Set plot parameters
    axes.set_xlabel("X", fontsize = 14)
    axes.set_ylabel("Y", fontsize = 14)
    axes.set_title("Random Walk", fontsize = 24)
    axes.tick_params(axis = "both", labelsize = 14)

    # Remove the axes lines 

    axes.get_xaxis().set_visible(False)
    axes.get_yaxis().set_visible(False)
    # Show the plot
    plt.show()

    keep_running = input("Do you wish to continue?(y/n)")
    if keep_running == "n":
        break
